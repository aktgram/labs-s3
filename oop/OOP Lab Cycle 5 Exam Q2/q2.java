import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import javax.swing.*;
import java.text.DecimalFormat;

public class q2 implements ActionListener {
    
    JFrame frame;
    JPanel pane;
    JLabel name;
    JButton convert;
    JTextField kmph, mps;
    
    public q2() {
        
        frame = new JFrame("SpeedConverter");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        
        name = new JLabel("by Sudev Suresh Sreedevi", SwingConstants.CENTER);
        frame.add(name, BorderLayout.SOUTH);
        
        pane = new JPanel();
        frame.add(pane);
        
        kmph = new JTextField("0");
        kmph.setColumns(11);
        pane.add(kmph);
        
        mps = new JTextField("");
        mps.setColumns(11);
        mps.setEditable(false);
        pane.add(mps);
                
        convert = new JButton("Convert");
        
        convert.addActionListener(this);
        
        pane.add(convert);
        
        frame.setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e) {
        
        double kh = Double.parseDouble(kmph.getText());
        double ms = kh * ((5.0/18));
        DecimalFormat df = new DecimalFormat (" 0.000 ");
        mps.setText(""+ df.format (ms)) ;
    }
    

    
    public static void main(String args[]) {
        
        new q2();
    }    
}