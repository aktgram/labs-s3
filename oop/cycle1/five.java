import java.util.Arrays; 
import java.util.Scanner;

class five{
    public static void main(String[] args) {
        
        int i, j, n;
        Scanner input = new Scanner(System.in);    
        System.out.println("How many elements?");
        n = input.nextInt();
        int[] arr = new int[n];

        for (i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        
        Arrays.sort(arr);
    
        for (j = 1; arr[0] == arr[j] ; j++);
      
        System.out.println("Second smallest element is : "+arr[j]);
    }
}
