import java.util.Scanner;

class four{
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        
        System.out.print("Enter a string to find reverse: ");
        String string = input.nextLine();
        String rev = ""; 
        for (int i = 0, j = string.length()-1 ; i < string.length(); i++, j--) {
           rev = rev + string.charAt(j); 
        }
        
        System.out.println("Reverse is: "+rev);
        input.close();
    }
}
