import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        Scanner input = new Scanner(System.in);
        String str = input.nextLine();
        int length = str.length();
        if(length >= 8) {
            int i, f=0, spl = 0, cptl = 0, sml = 0, dgts = 0;
            for(i = 0; i<length; i++) {
                char c = str.charAt(i);
                if(Character.isWhitespace(c)) {
                    f = 1;
                    break;
                }
                else if(Character.isDigit(c)) {
                    dgts++;
                }
                else if(Character.isUpperCase(c)) {
                    cptl++;
                }
                else if(Character.isLowerCase(c)) {
                    sml++;
                }
                else if(c == '@' || c == '$' || c == '&' || c == '%' || c == '#' ) {
                    spl++;
                }
            }
            if(f == 0 && cptl>=1 && sml>=1 && dgts == 2 && spl>=1 ) {
                System.out.println("Password is valid: "+str);
            }
            else {
                System.out.println("Password is not valid: "+str);
            }
        }
        else {
            System.out.println("Password is not valid: "+str);
        }
    }
}
