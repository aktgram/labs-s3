import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int arr[] = new int[n];
        int i;
        for( i=0; i<n; i++ ) {
            arr[i] = input.nextInt();
        }
        int sum = input.nextInt();
        System.out.print("Given Array: [");
        for( i=0; i<n; i++ ) {
            System.out.print(arr[i]);
            if(i != (n-1)) {
                System.out.print(", ");
            } 
        }
        System.out.println("]");
        System.out.println("Given sum : "+sum);
        System.out.println("Integer numbers, whose sum is equal to value : "+sum);
        for(i = 0; i<n; i++) {
            int j;
            for(j=i+1; j<n; j++) {
                if(arr[i] + arr[j] == sum ) {
                    System.out.println(arr[i]+", "+arr[j]);
                }
            }
        }
    }
}
