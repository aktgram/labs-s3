import java.util.Scanner;

class one{
    public static void main(String[] args) {

       Scanner input = new Scanner(System.in);

       System.out.print("Enter a no: ");

       int no = input.nextInt();
       int k = 0;

       for(int i=2; i<no/2; i++){
           if(no%i==0){
                k = 1;
                break;
           }        
       }
       if(k == 0 && no > 1){
           System.out.println("Prime");
       }
       else if(no != 2){
           System.out.println("Not Prime");
       }
       input.close();
    }
}
