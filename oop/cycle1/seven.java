import java.util.Scanner;  

public class seven {  
    public static void main(String[] args) { 
        int m, n, i, j;
        int[][] array = new int[25][25];
        Scanner input = new Scanner(System.in);  
        System.out.print("Enter the size of matrix : ");  
        m = input.nextInt();
        n = input.nextInt();
        System.out.println("Enter the elements : ");  
        for(i = 0; i<m; i++)
        {
            for(j = 0; j<n; j++)
            {
                array[i][j]=input.nextInt();  
            }
        }
        System.out.println("Transpose of the matrix is : ");
        for(j = 0; j<n; j++)
        {
            for(i = 0; i<m; i++)
            {
                System.out.print(array[i][j]+" ");
            }
            System.out.println("");
        }
    }
}