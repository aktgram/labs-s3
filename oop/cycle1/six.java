import java.util.Scanner;  

public class six {  
    public static void read(int [][]array, int r, int c) {
        int i,j;
        Scanner input = new Scanner(System.in);
        for( i=0; i<r; i++)
        {  
            for( j= 0; j<c; j++)
                array[i][j]=input.nextInt();  
        }
    }
    public static void main(String[] args) { 
        int m, n, p, q, i, j, k;
        int[][] array = new int[25][25];
        int[][] array2 = new int[25][25];
        Scanner input = new Scanner(System.in);  
        System.out.print("Enter the size of matrix1 : ");  
        m = input.nextInt();
        n = input.nextInt();
        System.out.println("Enter the elements : ");  
        read(array, m, n);
        System.out.print("Enter the size of matrix2 : ");  
        p = input.nextInt();
        q = input.nextInt();
        System.out.println("Enter the elements : ");  
        read(array2, p, q);
        if(n==p)
        {
            int [][] array3 = new int[25][25];
            for(i = 0; i<m; i++)
            {
                for(j = 0; j<q; j++)
                {
                    array3[i][j] = 0;
                    for(k=0; k<n; k++)
                        array3[i][j] += array[i][k]*array2[k][j];
                }
            }
            System.out.println("Resultant of Matrix multiplication is :");
            for(i = 0; i<m; i++)
            {
                for(j = 0; j<q; j++)
                {
                    System.out.print(array3[i][j]+" ");
                }
                System.out.println("");
            }
        }
        else
            System.out.println("Matrices cannot be multiplied!"); 

        input.close(); 
    }
}
