import java.util.Scanner;

class three{
    public static void main(String[] args) {
        int num = 0;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a string : ");
        String str = input.nextLine();

        System.out.print("Enter the character to search : ");
        char search = input.next().charAt(0);
        
        for(int i = 0; i < str.length()-1; i++) {
            if(search == str.charAt(i)) 
                num++;
        
        }
        System.out.print("The number of times is : ");
        System.out.println(num);
        input.close();
    }
}
