import java.util.Scanner;

class two{
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter a String : ");
        String string = input.nextLine();
       
        boolean isPali = true;
        int i = 0; 
        int j = string.length()-1;
     
        while(i<j){
            if(string.charAt(i) != string.charAt(j)){
                isPali = false;
            }
            i++;
            j--;
        }

        if(isPali){
            System.out.println("Palindrome");
        }
        else{
            System.out.println("Not Palindrome");
        }

        input.close();
    }
}
