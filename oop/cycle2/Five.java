interface Fruit {
    void color();
}

interface Brand {
    void brandName();
}

interface Orange {
    default void type() {
        System.out.println("Type is orange");
    }
}

interface Apple extends Fruit, Brand {
    void region();
}

class Plantation implements Apple, Orange {

    public void region() {
        System.out.println("Region is shimla");
    }

    public void color() {
        System.out.println("Color is Red");
    }

    public void brandName () {
        System.out.println("Brand is Himalayan");
    }

}

public class Five {
    public static void main(String args[]) {
        Plantation pl = new Plantation();
        pl.type();
        pl.color();
        pl.brandName();
        pl.region();
    }
}
