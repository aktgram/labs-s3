abstract class Shape {
    abstract void numberOfSides();
}

class Rectangle extends Shape {
    void numberOfSides() {
        System.out.println("No of sides Rectangle is 4");
    }
}

class Triangle extends Shape {
    void numberOfSides() {
        System.out.println("No of sides Triangle is 3");
    }    
}

class Hexagon extends Shape {
    void numberOfSides() {
        System.out.println("No of sides Hexagon is 6");
    }
}

public class Four {
    public static void main(String[] args) {
        Rectangle rect = new Rectangle();
        Triangle tr = new Triangle();
        Hexagon hexa = new Hexagon();

        rect.numberOfSides();
        tr.numberOfSides();
        hexa.numberOfSides();
    }    
}
