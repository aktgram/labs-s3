class Employee
{
    int Salary=10000;

    void display()
    {
        System.out.println("Name of the class is "+getClass().getName());
    }

    void calcSalary()
    {
        System.out.println("Salary of the Employee is 10000");
    }
}

class Engineer extends Employee
{
    void calcSalary()
    {
        System.out.println("Salary of the Employee is 20000");
    }
}

public class Inheritance
{
    public static void main(String[] args)
    {
        Engineer eng = new Engineer();
        eng.display();
        eng.calcSalary();
    }
}