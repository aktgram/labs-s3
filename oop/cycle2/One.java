import java.util.*;

public class One {

    static double area(float r) {
        return 3.14 * r * r; 
    }

    static double area(float l, float b) {
        return l * b; 
    }
    
    static double area(float a, float b, float c) {
        
        float s = (a+b+c)/2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c)); 
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter radius of the circle: ");
        float r = input.nextFloat();
        System.out.println("Area of circle is: "+area(r)+" units");

        System.out.println("Enter length and breadth of rectangle:");
        float l = input.nextFloat();
        float b = input.nextFloat();
        System.out.println("Area of rectangle is: "+area(l,b)+" units");
        

        System.out.println("\nEnter the dimensions of the triangle:");
        float x = input.nextFloat();
        float y = input.nextFloat();
        float z = input.nextFloat();
        System.out.println("Area of triangle is: "+area(x,y,z)+" units");

        input.close();
    }
}