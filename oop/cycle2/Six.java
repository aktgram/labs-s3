public class Six {

    protected void finalize() {
        System.out.println("Garbage collector called for object");
    }
    public static void main(String[] args) {
        Six ob1 = new Six();
        Six ob2 = new Six();
        Six ob3 = new Six();

        ob1 = null;
        ob2 = ob3;

        System.gc();
    }
}
