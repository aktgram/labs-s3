class Employee {

    public void calcSalary() {
        System.out.println("Salary of employee is 10000");
    }

    public void display() {
        System.out.println("Name of class is Employee");
    }
}

class Engineer extends Employee {
    public void calcSalary() {
        System.out.println("Salary of employee is 20000");
    }
}

public class Three {
    public static void main(String[] args) {
        Engineer engg = new Engineer();
        engg.display();
        engg.calcSalary();
    }
}
