import java.util.*;

class Employee {
    int age;
    float salary;
    String name, phoneNo, address;

    public void printSalary() {
        System.out.println("Salary: "+salary);
    }
}

class Officer extends Employee {
    String specialization;
}

class Manager extends Employee {
    String department;
}

public class Two {

    public static void main(String[] args) {
      
        Scanner input = new Scanner(System.in);

        Officer ofc = new Officer();
        Manager mgr = new Manager();

        System.out.println("Enter officer details");
        System.out.print("name? ");
        ofc.name = input.nextLine();
        System.out.print("age? ");
        ofc.age = input.nextInt();
        System.out.print("salary? ");
        ofc.salary = input.nextFloat();
        input.nextLine();
        System.out.print("phoneNo? ");
        ofc.phoneNo = input.nextLine();
        System.out.print("address? ");
        ofc.address = input.nextLine();
        System.out.print("specialisation? ");
        ofc.specialization = input.nextLine();

        System.out.println("Enter manager details");
        System.out.print("name? ");
        mgr.name = input.nextLine();
        System.out.print("age? ");
        mgr.age = input.nextInt();
        System.out.print("salary? ");
        mgr.salary = input.nextFloat();
        input.nextLine();
        System.out.print("phoneNo? ");
        mgr.phoneNo = input.nextLine();
        System.out.print("address? ");
        mgr.address = input.nextLine();
        System.out.print("department? ");
        mgr.department = input.nextLine();

        input.close();

        System.out.println("\nEmployee Details");
        System.out.println("\nOfficer");
        System.out.println("Name: " + ofc.name);
        System.out.println("Age: " +  ofc.age);
        System.out.println("Phone Number: " + ofc.phoneNo);
        System.out.println("Address: " + ofc.address);
        System.out.println("\nSpecialization: " + ofc.specialization);
        ofc.printSalary();

        System.out.println("\nManager Details");
        System.out.println("Name: " + mgr.name);
        System.out.println("Age: " +  mgr.age);
        System.out.println("Phone Number: " + mgr.phoneNo);
        System.out.println("Address: " + mgr.address);
        System.out.println("\nDepartment: " + mgr.department );
        mgr.printSalary();

  }  
}
