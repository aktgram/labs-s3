import java.io.*;
import java.util.*;

class Five {
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Enter line of integers");
        
        
        String integers = in.nextLine();
        StringTokenizer str = new StringTokenizer(integers);
        int sum = 0;

        while (str.hasMoreTokens()) {
            int i = Integer.parseInt(str.nextToken());
            System.out.println(i);
            sum += i;
        }
        System.out.println("Sum: " + sum);
    }
}