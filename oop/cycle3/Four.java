import java.io.*;
import java.util.*;

class Four {
    public static void main(String[] args) throws IOException {
        
        Scanner in = new Scanner(System.in);
        System.out.println("Name of file to read from?");
        String name = in.nextLine();
        System.out.println("Name of file to write to?");
        String newFile = in.nextLine();
        
        try {
            FileReader fin = new FileReader(name);
            FileWriter fout = new FileWriter(newFile);

            System.out.println("\nPrinting contents of "+name+" to screen as well as to "+newFile);
            int ch;
            while((ch = fin.read()) != -1) {
                System.out.print((char)ch);
                fout.append((char)ch);
            }
        
            System.out.println();

            fin.close();
            fout.close();

        } 
        catch (FileNotFoundException e) {
            System.out.println("Error: "+ e);
        }
        catch (IOException e) {
            System.out.println("Error: "+ e);
        }
        catch (Exception e) {
            System.out.println("Error: "+e);
        }
    }
}
