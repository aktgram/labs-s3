import java.io.*;

class One {
    public static void main(String[] args) throws IOException {
        
        try {
            FileWriter fout = new FileWriter("hello.txt");
            String str = "Hello world\nWelcome to Java programming\n";
            str += "Java is an oop language";
            fout.write(str);
            fout.close();
            System.out.println("Written string to hello.txt");
            
            FileReader fin = new FileReader("hello.txt");
            System.out.println("Reading from hello.txt");
            int ch;
            while( (ch = fin.read()) != -1) {
                System.out.print((char)ch);
            }
            
            System.out.println();
            fin.close();

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }        
    }
}
