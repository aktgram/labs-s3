import java.io.*;
import java.util.*;

class Three {
    public static void main(String[] args) throws IOException {
        
        Scanner in = new Scanner(System.in);
        System.out.println("Name of file to read from?");
        String name = in.nextLine();
        
        try {
            FileReader fin = new FileReader(name);
            int ch = fin.read();
            int characters = 0, words = 0, lines = 0;

            System.out.println("Checking in "+name);

            if (ch != -1) {  //handling empty files and first line
                lines++;
                characters++;
                words++;
            }

            while ((ch = fin.read()) != -1) {
                characters++;
                if (ch == '\n') {
                    lines++;
                    words++;
                }
                if (ch == ' ' || ch == '\t') {
                    words++;
                }
            }
            fin.close();
            System.out.println("Characters: "+characters);
            System.out.println("Words: "+words+"\nLines: "+lines);

        } catch (Exception e) {
            System.out.println("Error: "+e);
        }
    }
}