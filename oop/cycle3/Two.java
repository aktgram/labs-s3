import java.io.*;

class Two {
    public static void main(String[] args) throws IOException {
        
        try {
            FileReader fin = new FileReader("hello.txt");
            System.out.println("Reading from hello.txt");
            
            int ch = fin.read() , i = 1;

            if (ch != -1) {  //handling if empty file
                System.out.print("1 " + (char)ch);
            }

            while ( (ch = fin.read()) != -1 ) {
                System.out.print((char)ch);
                if ((char)ch == '\n') {
                    System.out.print(++i + " ");
                }
            }

	        System.out.println();
            fin.close();
        }
        catch (Exception e) {
            System.out.println("Error: "+e);
        }
    }
}
