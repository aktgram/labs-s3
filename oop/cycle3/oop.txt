\documentclass[14pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper, total={6in, 8in}]{geometry}
\usepackage{listings}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\begin{document}
\include{mytitle}
 
\begin{titlepage}
	\begin{center}
	\fontsize{14pt}{14pt} \selectfont {College of Engineering Trivandrum}\\
	\vspace*{0.8cm}
	\vspace*{1.5cm}	
	\fontsize{18pt}{1cm}\selectfont\textbf{{Object Oriented Programming Lab}}\\
	\vspace*{0.8cm}
	\vspace*{1.5cm}
	\includegraphics[width=2in]{cet emblem.jpg}\\
	\vspace*{2.5cm}
	\fontsize{14pt}{14pt} \selectfont{ Aishwarya A J}\\
	\vspace{0.2cm}
	\fontsize{14pt}{14pt} \selectfont{S3 CSE Roll No:6}\\
	\vspace*{0.2cm}
	\fontsize{14pt}{14pt} \selectfont{TVE19CS006}\\
	\vspace*{2cm}
	\fontsize{18pt}{1cm}\selectfont {Department Of Computer Science}\\
	\vspace{0.5cm}
		\fontsize{18pt}{18pt}\selectfont{October 21,2020}
\end{center}
\end{titlepage}
%\end{document} 

%\begin{document}
%\maketitle
\newpage
\tableofcontents
\newpage
\noindent
%\begin{flushleft}
\includegraphics[width=0.5in]{cet emblem 1.jpg}
%\end{flushleft}
\hfill
%\begin{center}
  \textbf{ \huge{\underline{CSL 203-Object Oriented Programming Lab }}}
%\end{center}
\begin{center}
   \vspace{0.5cm}
    \fontsize{18pt}{18pt}\textbf{Cycle 3}
\end{center}
 

\section{File handling program in Java with reader/writer}

\subsection{Aim}
Write a file handling program in Java with reader/writer.

\subsection{Code}
\hline
\begin{lstlisting}[language=java,showstringspaces=false]
import java.io.*;
class File {
    public static void main(String[] args) {
        try{
            FileWriter f1=new FileWriter("file1.txt");
            String s="Java is a class-based, object-oriented programming language.\n"
            +"It is designed to have as few implementation dependencies as possible.
            \n"+"It is a general-purpose programming language intended for 
            application development \n"+"Java applications are typically compiled 
            to bytecode that can run on any Java virtual machine.\n"+
            "The syntax of Java is similar to C and C++, but has fewer low-level 
            facilities \n"+"The Java runtime provides dynamic capabilities that are 
            not available in traditional compiled languages.\n";
            f1.write(s);
            System.out.println("\nWriting Successful!!\n\n");
            f1.close();
            FileReader f2=new FileReader("file1.txt");
            int ch;
            System.out.println("Reading File...\n");
            while((ch=f2.read())!=-1){
                System.out.print((char)ch);
            } 
            f2.close();
        }
        catch (Exception e){
            System.out.println("Error:"+e);
        }   
    }
}
\end{lstlisting}
\hline
\subsection{Sample output}
\includegraphics[width=\textwidth]{1File.png}
\newpage
\section{Area of shapes using method overloading.}

\subsection{Aim}
Write a Java program that reads a file and displays the file on the screen, with a line number 
before each line.

\subsection{Code}
\hline
\begin{lstlisting}[language=java,showstringspaces=false]
import java.io.*;
import java.util.Scanner;
public class FileDisplay {
    public static void main(String[] args) throws IOException {
        Scanner s=new Scanner(System.in);
        System.out.print("Enter File name:");
        String f=s.next();
        try {
            FileReader f1=new FileReader(f);
            int ch,count=1;
            if (f.length()!=0)
               System.out.print(count+"  ");
            while((ch=f1.read())!=-1){
                System.out.print((char)ch);
                if((char)ch=='\n'){
                    System.out.print("\n" + ++count +"  ");
                }
            } 
            f1.close(); 
            System.out.println();        
        }
        catch (FileNotFoundException e){
            System.out.println("Error:"+e);
        }     
    }
}

\end{lstlisting}
\hline
\subsection{Sample output}
\includegraphics[width=\textwidth]{2filedisplay.png}
\newpage
\section{Program to display the number of characters, lines and words in a text
file}

\subsection{Aim}
Write a Java program that displays the number of characters, lines and words in a text
file.

\subsection{Code}
\hline
\begin{lstlisting}[language=java,showstringspaces=false]

import java.io.*;
import java.util.Scanner;
public class FileCount{
    public static void main(String[] args) throws IOException{
        Scanner s=new Scanner(System.in);
        System.out.print("Enter File name:");
        String f=s.next();
        System.out.println();
        try(FileReader f1=new FileReader(f)) {
            int ch,wc=0,cc=0,lc=0;
            while((ch=f1.read())!=-1){
                System.out.print((char)ch);
                cc++;
                if((char)ch=='\n'){
                    lc++;
                    wc++;
                }
                if((char)ch==' ')
                    wc++;
            } 
            System.out.println("\n\n-------------------------");
            System.out.println("| Character Count = "+ cc +" |");
            System.out.println("| Word Count \t  = "+wc + "\t|");
            System.out.println("| Line Count \t  = " +(lc+1)+ "\t|");
            System.out.println("-------------------------");
        }
        catch (FileNotFoundException e){
            System.out.println("Error:"+e);
        }     
    }
}
\end{lstlisting}
\hline
\subsection{Sample output}
\includegraphics[width=\textwidth]{filecountwc.png}
\newpage
\section{Area of shapes using method overloading.}

\subsection{Aim}
Write a Java program that read from a file and write to file by handling all file related 
exceptions.

\subsection{Code}
\hline
\begin{lstlisting}[language=java,showstringspaces=false]

import java.util.Scanner;
class Area{
    static Double area(double x){
        return 3.14*x*x;
    }
    static Float area(float x,float y){
         return x*y;
    }
    static Double area(double x, double y){
        return 0.5*x*y;
    }
    
}
class shapes{
    public static void main(String[] args) {
        float l,w;
        double b,h,r;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the radus of the circle:");
        r= s.nextFloat();
        System.out.print("Enter the length and breadth of the rectangle:");
        l= s.nextFloat();
        w= s.nextFloat();
        System.out.print("Enter the base and height of the triangle:");
        b= s.nextDouble();
        h= s.nextDouble();
        System.out.println("\nArea of the circle = "+Area.area(r)+" sq. units");
        System.out.println("Area of the rectangle = "+Area.area(l,w)+" sq. units");
\end{lstlisting}
\hline
\subsection{Sample output}
\includegraphics[width=\textwidth]{1Area.png}
\newpage
\section{Area of shapes using method overloading.}

\subsection{Aim}
Write a Java program that reads a line of integers, and then displays each integer, and the
sum of all the integers.

\subsection{Code}
\hline
\begin{lstlisting}[language=java,showstringspaces=false]
import java.util.StringTokenizer;
import java.util.Scanner;

public class IntegerSum {
    public static void main(String args[]) {
        int sum = 0;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter a line of integers: ");
        StringTokenizer st = new StringTokenizer(s.nextLine());
        while (st.hasMoreTokens()) {
            int i = Integer.valueOf(st.nextToken());
            System.out.println(i);
            sum += i;
        }
        System.out.println("Sum of integers = " + sum);
    }
}
\end{lstlisting}
\hline
\subsection{Sample output}
\includegraphics[width=\textwidth]{IntegerSum.png}
\newpage
\end{document}