class OddThread extends Thread{
    public void run(){

        for(int i=1;i<=100;i++){
            if(i%2==1)
               System.out.println("odd:\t"+i);
            else{
                EvenThread t = new EvenThread(i);
                t.start();
            }
            try{
                Thread.sleep(10);
            }
            catch(InterruptedException ex){
                System.out.println(ex);
            }
        }
    }
}

class EvenThread extends Thread{
    int n;
    EvenThread(int n){
        this.n = n;
    }
    synchronized public void run(){
        System.out.println("even:\t"+n);
    }
}

public class Five {
    public static void main(String[] args) {
        OddThread t = new OddThread();
        t.start();
    }
}