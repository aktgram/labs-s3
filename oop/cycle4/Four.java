class ThreadInfo {
    synchronized void print(){
        Thread t= new Thread();
        System.out.println("Thread Name: "+ t.getName());
        System.out.println("Thread Id: "+ t.getId());
        System.out.println("Thread Priority: "+ t.getPriority()+"\n");
        try {
          Thread.sleep(400);
        } 
        catch (InterruptedException e) {
          System.out.println(e);
        }
    }
  }
  
  public class Four {
    public static void main(String args[]) {
      
        ThreadInfo obj = new ThreadInfo();
        Thread t1 = new Thread(){
            public void run(){
                obj.print();
            }
        };
    
        Thread t2 = new Thread(){
            public void run(){
                obj.print();
            }
        };
        t1.start();
        t2.start();
    }
  }