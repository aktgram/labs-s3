import java.util.*;
import java.io.*;

public class One {
    public static void main(String[] args) throws Exception{
        int a,b;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Enter 2 numbers: ");
        a = in.nextInt();
        b = in.nextInt();

        try{
            System.out.println("\"try block\"");
            System.out.println(a+"/"+ b+" = " +a/b);
        }
        catch(ArithmeticException e){
            System.out.println("\"catch block\"");
            e.printStackTrace();
        }
        finally{
            System.out.println("\"finally block\"");
        }

        System.out.println("\nNow Outside the try-catch-finally\n");
        //to demonstrate throw keyword
        throw new IOException();
    }
}