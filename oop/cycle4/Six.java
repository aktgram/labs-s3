class PriorityThread extends Thread{  
    public void run(){    
       System.out.println("thread name : "+Thread.currentThread().getName()+
       "\tthread priority : "+Thread.currentThread().getPriority());  
    }  
}

public class Six {
    public static void main(String args[]){  
        
        PriorityThread p1=new PriorityThread();  
        PriorityThread p2=new PriorityThread();  

        p1.setPriority(Thread.MIN_PRIORITY);  
        p2.setPriority(Thread.MAX_PRIORITY);  
        
        p1.start();  
        p2.start(); 
    }
}     