import java.util.Random;

class Square extends Thread {
	int n;

	Square(int randomNumbern) {
		n = randomNumbern;
	}

	public void run() {
        System.out.println("Square of " + n+ " = " + (n * n));
	}
}

class Cube extends Thread {
	int n;

	Cube(int randomNumber) {
		n = randomNumber;
	}

	public void run() {
		System.out.println("Cube of " + n + " = " + n * n * n);
	}
}

class RandomNo extends Thread {
	public void run() {

		Random rndm = new Random();
        
        for (int i = 0; i < 10; i++) {
            
            int r = rndm.nextInt(100);
			System.out.println("New Random Integer : " + r);
            
            if((r%2) == 0) {
				Square s = new Square(r);
				s.start();
			}
			else {
				Cube c = new Cube(r);
				c.start();
			}
			try {
                //sleep for 1 second
				Thread.sleep(1000);
			} 
			catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}
}

public class Three{
	public static void main(String args[]) {

        RandomNo rn = new RandomNo();
        rn.start();
	}
}
