import java.util.*;

class NegativeException extends Exception {
    
    public NegativeException(String s) {
        super(s);
    }
}

public class Two {
    static void check(int n) throws NegativeException {
        
        if(n<0)
            throw new NegativeException("Number is negative");
        else
            System.out.println("Non negative number");
    }

    public static void main(String[] args) {
    
        try {
            
            Scanner scan = new Scanner(System.in);
            
            int n;
            System.out.println("Enter a number (negative nos will be errored)");
            n=scan.nextInt();
            
            scan.close();
            check(n);
        }
        catch(NegativeException e) {
            e.printStackTrace();
        }
    }
}