import java.util.*;

public class cycle4_lab_exam implements Runnable {

    private final Object leftChopstick;
    private final Object rightChopstick;
    int eat = 0;

    cycle4_lab_exam(Object left, Object right) {
        this.leftChopstick = left;
        this.rightChopstick = right;
    }

    private void doAction(String action) throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " " + action);
        Thread.sleep(((int) (Math.random() * 100)));
    }

    public void run() {
        try {
            while (eat == 0) {
                doAction(": Thinking"); // thinking
                synchronized (leftChopstick) {
                    doAction(": Picked up left Chopstick");
                    synchronized (rightChopstick) {
                        doAction(": Picked up right Chopstick - eating"); // eating
                        eat++;
                        doAction(": Put down right Chopstick");
                    }
                    doAction(": Put down left Chopstick. Returning to thinking");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    
    public static void main(String[] args) throws Exception {
        
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        cycle4_lab_exam[] philosophers = new cycle4_lab_exam[n];
        Object[] Chopsticks = new Object[philosophers.length];

        for (int i = 0; i < Chopsticks.length; i++) {
            Chopsticks[i] = new Object();
        }

        for (int i = 0; i < philosophers.length; i++) {

            Object leftChopstick = Chopsticks[i];
            Object rightChopstick = Chopsticks[(i + 1) % Chopsticks.length];

            if (i == philosophers.length - 1) {
                philosophers[i] = new cycle4_lab_exam(rightChopstick, leftChopstick); // The last cycle4_lab_exampicks up the right Chopstick first
            } else {
                philosophers[i] = new cycle4_lab_exam(leftChopstick, rightChopstick);
            }

            Thread t = new Thread(philosophers[i], "Philosopher" + (i + 1));
            t.start();
        }
    }
}
