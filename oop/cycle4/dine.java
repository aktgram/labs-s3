import java.util.Scanner;

class Chopstick
{

	public boolean used;
	public Chopstick(){
	}

	public synchronized void take() {
//		System.out.println ("Used :: " + name );
		this.used = true;
	}
	public synchronized void release() {
//		System.out.println ("Released :: " + name );
		this.used = false ;
	}
}
// State : 2 = Eat, 1 = think
class Philosopher extends Thread
{
	private Chopstick leftChopistick;
	private Chopstick rightChopistick;

	private String name;
	private int state;
	private int rounds;

	public Philosopher ( String name, Chopstick left, Chopstick right, int rounds){
		this.name = name;
		leftChopistick = left;
		rightChopistick = right;
		rounds = rounds;
		this.think();
	}
 
	public void eat()
	{
		if(! leftChopistick.used)
		{
			if(!rightChopistick.used)
			{
				leftChopistick.take();
				System.out.println(name + " Picked up Left chopstick");

				rightChopistick.take();
				System.out.println(name + " Picked up Right chopstick - eating");
				this.state = 2;
				try{
					Thread.sleep(1000);
				}
				catch(InterruptedException ex){ }
				leftChopistick.release();
				rightChopistick.release();
				System.out.println(name+" Put down left chopstick - Back to thinking");
		 		System.out.println(name+" Put down right chopstick");
			}
		}
		this.state = 1;	
		try{
			Thread.sleep(1000);
		}
		catch(InterruptedException ex){}
	}

	public void think(){
		 	this.state = 1;
		 	System.out.println(name + " Thinking");
			try{
				Thread.sleep(1000);
			}
			catch(InterruptedException ex){}
	}

	public void run(){
		
		for(int i=0; i<=rounds; i++){
			eat();
		}
	}
}

public class dine
{
	public static void main(String[] args)
	{
		System.out.println("Program to Do the Dining Philosophers Problem in Java\n\n Harishankar S Kumar, TVE19CS034\n");
		Scanner in = new Scanner(System.in);
		int rounds=10;
		System.out.print("Enter number of Philosophers : ");
 		int number = in.nextInt();
		Chopstick[] stick = new Chopstick[number];

		//initlize the stick
		for(int i=0; i< number; i++){
			stick[i] = new Chopstick();
		}
		Philosopher[] philosophers = new Philosopher[number];
		for(int i=0; i<number-1; i++){
			philosophers[i] = new Philosopher("Philosopher "+(i+1), stick[i], stick[i+1], rounds);
		}
		philosophers[number-1] = new Philosopher("Philosopher "+(number), stick[0], stick[number-1], rounds);
		for(int i=0;i<number;i++){
//			System.out.println("Thread "+ i + " has started");
			Thread t= new Thread(philosophers[i]);
			t.start();
		}
 	}
}