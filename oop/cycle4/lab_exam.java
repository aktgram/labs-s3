import java.util.*;

class Think extends Thread {

    int number;
    Think(int n) {
        number = n;
    }
    synchronized void think(int number) {
        System.out.println("Philosopher "+number+": thinking");
        try {
            Thread.sleep(400);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        think(number);
    }
}


class PickFork {

    int number;

    PickFork(int number) {
        this.number = number;
    }

    synchronized void pick() {
        System.out.println("Philosopher "+number+": Picked up left chopstick");
        System.out.println("Philosopher "+number+": Picked up right chopstick - eating");
        try {
            Thread.sleep(400);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    synchronized void drop() {
        System.out.println("Philosopher "+number+": Put down left chopstick - Back to thinking");
        System.out.println("Philosopher "+number+": Put down right chopstick");
        try {
            Thread.sleep(400);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class OddPhilosoper extends Thread {

    int number,n;
    OddPhilosoper(int number, int n) {
        this.number = number;
        this.n = n;
    }

    public void run() {
        PickFork p = new PickFork(number);
  
        p.pick();
        p.drop();

        if(number-1 > 0) {
            EvenPhilosoper et = new EvenPhilosoper(number-1);
            et.start();
        }
    }
}

class EvenPhilosoper extends Thread {

    int number;
    EvenPhilosoper(int number) {
        this.number = number;
    }

    public void run() {
        PickFork p = new PickFork(number);

        p.pick();
        p.drop();
    }
}

public class lab_exam {
    public static void main(String[] args) {
        Scanner in  = new Scanner(System.in);
        int n = in.nextInt();

        for(int i=1; i<=n; i++) {
            Think t = new Think(i);
            t.start();
        }

        for(int i=1; i<=n; i+=2) {
            OddPhilosoper ot = new OddPhilosoper(i,n);

            ot.start();
        }
    }
}
