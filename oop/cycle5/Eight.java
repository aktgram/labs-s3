import java.sql.*; 
import java.util.*;
 
public class Eight {  
   public static void main(String[] args)throws Exception {
      Class.forName("com.mysql.cj.jdbc.Driver");
      try (
         
         Scanner input = new Scanner(System.in);
         Connection conn = DriverManager.getConnection(
               "jdbc:mysql://localhost:3306/java_cycle5",
               "ajayk", "password123");

         Statement stmt = conn.createStatement();
         ) {
            
            System.out.println("Enter table name");
            String table_name = input.nextLine();
            try {
               String create = "CREATE TABLE "+table_name
                              +"(rollno INT, name VARCHAR(20), CGPA DOUBLE);";
               stmt.executeUpdate(create);
            }
            catch (Exception e) {
               System.out.println("Table exists, now enter data");
            }
            System.out.println("Enter number of students, n");

            int n = input.nextInt( );
            int i=0;
            while(i<n)
            {
               System.out.println("Enter roll no, name and CGPA of student");
               int rollno = input.nextInt( );
               String name = input.next();
               double CGPA = input.nextDouble();

               String insert = "INSERT INTO "+table_name+
                              " (rollno,name,CGPA) VALUES ("
                                 +rollno +",'"+name+"',"+CGPA+");";
               stmt.executeUpdate(insert);
               ++i;
            }
            String strSelect = "select * from "+table_name+" where CGPA>7";
            System.out.println("The SQL statement is: " + strSelect + "\n");
    
            ResultSet rset = stmt.executeQuery(strSelect);
            int rowCount = 0;
            while(rset.next()) { 
                int rollno = rset.getInt("rollno");
                String name = rset.getString("name");
                double CGPA = rset.getDouble("CGPA");
                System.out.println(rollno + ", " + name + ", " + CGPA);
                ++rowCount;
            }
            System.out.println("Total number of records = " + rowCount);
      } catch(SQLException ex) {
         ex.printStackTrace();
      }
   }
}