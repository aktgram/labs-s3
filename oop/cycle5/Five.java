import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;
import java.text.DecimalFormat;

public class Five extends JFrame implements ActionListener
{
    JFrame f = new JFrame("Calculator");
    JTextField t = new JTextField();
    Double a,b,result;
    char operator;

    JButton b0 = new JButton("0");
    JButton b1 = new JButton("1");
    JButton b2 = new JButton("2");
    JButton b3 = new JButton("3");
    JButton b4 = new JButton("4");
    JButton b5 = new JButton("5");
    JButton b6 = new JButton("6");
    JButton b7 = new JButton("7");
    JButton b8 = new JButton("8");
    JButton b9 = new JButton("9");

    JButton dot = new JButton(".");
    JButton add = new JButton("+");
    JButton sub = new JButton("-");
    JButton mul = new JButton("X");
    JButton div = new JButton("/");
    JButton equal = new JButton("=");
    JButton clear = new JButton("C");
    JButton del = new JButton("DEL");

    Five()
    {
        f.setSize(600,710);
        f.setResizable(false);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        t.setBounds(10,10,565,100);
        t.setHorizontalAlignment(JTextField.RIGHT);

        b0.setBounds(140,450,120,100);
        b1.setBounds(10,340,120,100);
        b2.setBounds(140,340,120,100);
        b3.setBounds(270,340,120,100);
        b4.setBounds(10,230,120,100);
        b5.setBounds(140,230,120,100);
        b6.setBounds(270,230,120,100);
        b7.setBounds(10,120,120,100);
        b8.setBounds(140,120,120,100);
        b9.setBounds(270,120,120,100);

        dot.setBounds(10,450,120,100);
        del.setBounds(270,450,120,100);
        clear.setBounds(10,560,380,100);
        equal.setBounds(400,560,175,100);
        add.setBounds(400,450,175,100);
        sub.setBounds(400,340,175,100);
        mul.setBounds(400,230,175,100);
        div.setBounds(400,120,175,100);        

        dot.setBackground(Color.LIGHT_GRAY);
        del.setBackground(Color.RED);
        clear.setBackground(Color.LIGHT_GRAY);

        add.setBackground(Color.GREEN);
        sub.setBackground(Color.GREEN);
        mul.setBackground(Color.GREEN);
        div.setBackground(Color.GREEN);     
        equal.setBackground(Color.GREEN);     
        
        Font text = new Font("Courier", Font.BOLD,30);

        b0.setFont(text);
        b1.setFont(text);
        b2.setFont(text);
        b3.setFont(text);
        b4.setFont(text);
        b5.setFont(text);
        b6.setFont(text);
        b7.setFont(text);
        b8.setFont(text);
        b9.setFont(text);
        dot.setFont(text);
        clear.setFont(text);
        del.setFont(text);
        equal.setFont(text);
        add.setFont(text);
        sub.setFont(text);
        mul.setFont(text);
        div.setFont(text);
        
        t.setFont(text);

        b0.addActionListener(this);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        b4.addActionListener(this);
        b5.addActionListener(this);
        b6.addActionListener(this);
        b7.addActionListener(this);
        b8.addActionListener(this);
        b9.addActionListener(this);
        dot.addActionListener(this);
        equal.addActionListener(this);
        clear.addActionListener(this);
        del.addActionListener(this);
        add.addActionListener(this);
        sub.addActionListener(this);
        mul.addActionListener(this);
        div.addActionListener(this);
        
        f.add(b0);
        f.add(b1);
        f.add(b2);
        f.add(b3);        
        f.add(b4);
        f.add(b5);
        f.add(b6);
        f.add(b7);
        f.add(b8);
        f.add(b9);

        f.add(dot);
        f.add(clear);
        f.add(add);
        f.add(sub);
        f.add(mul);
        f.add(div);
        f.add(equal);
        f.add(del);
        
        f.add(t);
        f.setLayout(null);
        f.setVisible(true);
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==b0)
            t.setText(t.getText().concat("0"));

        else if(e.getSource()==b1)
            t.setText(t.getText().concat("1"));

        else if(e.getSource()==b2)
            t.setText(t.getText().concat("2"));

        else if(e.getSource()==b3)
            t.setText(t.getText().concat("3"));

        else if(e.getSource()==b4)
            t.setText(t.getText().concat("4"));

        else if(e.getSource()==b5)
            t.setText(t.getText().concat("5"));

        else if(e.getSource()==b6)
            t.setText(t.getText().concat("6"));

        else if(e.getSource()==b7)
            t.setText(t.getText().concat("7"));

        else if(e.getSource()==b8)
            t.setText(t.getText().concat("8"));

        else if(e.getSource()==b9)
            t.setText(t.getText().concat("9"));

        else if(e.getSource()==dot)
        {
            String tmp = t.getText();
            boolean flag=false;

            for(int i=0;i<tmp.length();++i)
                if(tmp.charAt(i)=='.')
                {
                    flag=true;
                    break;
                }
            
            if(flag==false)
                t.setText(t.getText().concat("."));
        }

        else if(e.getSource()==add)
        {
            a=Double.parseDouble(t.getText());
            operator=1;
            t.setText("");
        } 
            
        else if(e.getSource()==sub)
        {
            a=Double.parseDouble(t.getText());
            operator=2;
            t.setText("");
        }
            
        else if(e.getSource()==mul)
        {
            a=Double.parseDouble(t.getText());
            operator=3;
            t.setText("");
        }
            
        else if(e.getSource()==div)
        {
            a=Double.parseDouble(t.getText());
            operator=4;
            t.setText("");
        }
            
        else if(e.getSource()==equal)
        {
            b=Double.parseDouble(t.getText());
            switch(operator)
            {
                case 1:result=a+b;
                       break;
            
                case 2:result=a-b;
                       break;
            
                case 3:result=a*b;
                       break;
            
                case 4:result=a/b;
                       break;
            
                default:result=0.0;
            }
            
            if(result.isInfinite())
                t.setText("ERROR");
            else
            {
                DecimalFormat df = new DecimalFormat("0.000");
                t.setText(""+df.format(result));
            }
        }            
            
        if(e.getSource()==del)
		{
			String s=t.getText();
			t.setText("");
			for(int i=0;i<s.length()-1;++i)
			    t.setText(t.getText()+s.charAt(i));
		}

        else if(e.getSource()==clear)
            t.setText("");
    }

    public static void main(String[] args)
    {
        new Five();
    }
}