import java.awt.*;
import javax.swing.*;
class One extends JPanel
{

    One() {
        JFrame f=new JFrame("Lines, Ovals & Rectangles");
        f.setDefaultCloseOperation(3);

        this.setBackground( Color.LIGHT_GRAY );
        f.add(this); 
        f.setSize(400, 410); 
        f.setVisible(true);
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(Color.red);        
        g.drawLine(5,30,350,30);
        g.setColor(Color.BLACK);
        g.drawOval(50,150,90,55);
        g.fillOval(200,150,90,55);
        g.setColor(Color.white);
        g.drawRect(50,60,90,55);
        g.fillRect(200,60,90,55);

    }
    public static void main(String[] args) {
        One d = new One();
    }
}