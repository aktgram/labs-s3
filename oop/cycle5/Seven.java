import java.sql.*; 
import java.util.*;
 
public class Seven {  
   public static void main(String[] args)throws Exception {
      Class.forName("com.mysql.cj.jdbc.Driver");
      
      try (

         Scanner in = new Scanner(System.in);
         Connection conn = DriverManager.getConnection(
               "jdbc:mysql://localhost:3306/java_cycle5",
               "ajayk", "password123");

         Statement stmt = conn.createStatement();
         
      ) {
         System.out.println("Enter table name");
         String table_name = in.nextLine();
         
         String strSelect = "select * from "+table_name;
 
         ResultSet rset = stmt.executeQuery(strSelect);

         System.out.println("The records selected are:");
         int rowCount = 0;
         while(rset.next()) { 
            int col1 = rset.getInt("id");
            String col2 = rset.getString("name");
            double col3   = rset.getDouble("age");
            System.out.println(col1 + ", " + col2 + ", " + col3);
            ++rowCount;
         }
         System.out.println("Total records = " + rowCount);

      } catch(SQLException ex) {
         ex.printStackTrace();
      }
   }
}