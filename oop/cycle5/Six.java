import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


public class Six extends JFrame implements ActionListener {
    JRadioButton buttonRed, buttonYellow, buttonGreen;

      Signal green = new Signal(Color.green);
      Signal yellow = new Signal(Color.yellow);
      Signal red = new Signal(Color.red);

    public Six(){
        super("Java Traffic Light Program");
        this.setDefaultCloseOperation(3);
        this.setSize(300, 400);
        getContentPane().setLayout(new FlowLayout());

        buttonRed = new JRadioButton("Red");
        buttonYellow = new JRadioButton("Yellow");
        buttonGreen = new JRadioButton("Green");
        ButtonGroup bg = new ButtonGroup();
        buttonRed.addActionListener(this);
        buttonYellow.addActionListener(this);
        buttonGreen.addActionListener(this);      

        green.turnOn(false);
        yellow.turnOn(false);
        red.turnOn(true);

        JPanel trafficPanel = new JPanel(new GridLayout(3,1));
        trafficPanel.add(red);
        trafficPanel.add(yellow);
        trafficPanel.add(green);
        JPanel lightPanel = new JPanel();

        bg.add(buttonRed);
        bg.add(buttonYellow);
        bg.add(buttonGreen);
        lightPanel.add(buttonRed);
        lightPanel.add(buttonYellow);
        lightPanel.add(buttonGreen);

        getContentPane().add(trafficPanel);
        getContentPane().add(lightPanel);
        
        }


    public static void main(String[] args){
        Six trafficLight = new Six();      
        trafficLight.setVisible(true);
    }  
    public void actionPerformed(ActionEvent e){      
        if (e.getSource() == buttonRed){
            green.turnOn(false);          
            yellow.turnOn(false);
            red.turnOn(true);
        } else if (e.getSource() == buttonYellow){
            yellow.turnOn(true);          
            green.turnOn(false);
            red.turnOn(false);
        } else if (e.getSource() == buttonGreen){
            red.turnOn(false);          
            yellow.turnOn(false);
            green.turnOn(true);
        }
    }
}  
class Signal extends JPanel{

    Color on;
    int radius = 40;
    int border = 10;
    boolean change;

    Signal(Color color){
        on = color;
        change = true;
    }

    public void turnOn(boolean a){
        change = a;
        repaint();      
    }

    public Dimension getPreferredSize(){
        int size = (radius+border)*2;
        return new Dimension( size, size );
    }

    public void paintComponent(Graphics graphics){
        graphics.setColor( Color.black );
        graphics.fillRect(0,0,getWidth(),getHeight());

        if (change){
            graphics.setColor( on );
        } else {
            graphics.setColor( on.darker().darker().darker() );
        }
        graphics.fillOval( border,border,2*radius,2*radius );
    }
}