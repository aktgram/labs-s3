import java.awt.*;  
import java.awt.event.*;

public class Three extends KeyAdapter
{  
        Label l;  
        TextArea area;  
        Frame f;  
        Three()
        {  
            f=new Frame("Key Adapter");
            
            Label l1=new Label();
            l1.setBounds(20,50,200,20);
            l1.setText("Press Esc key to Exit program");  
            
            l=new Label();
            l.setBounds(20,80,300,20);
            l.setText("No of Words: 0 No of Characters: 0"); 
            area = new TextArea();  
            area.setBounds(20,110,300,300);  
            area.addKeyListener(this);  
              
            f.add(l1);
            f.add(l);
            f.add(area);  
            f.setSize(420,420);  
            f.setLayout(null);  
            f.setVisible(true);  
        }  
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
        }
        public void keyReleased(KeyEvent e) 
        {  
            String text=area.getText();  
            String words[]=text.split("\\s+");  
            l.setText("No of Words: "+words.length+" No of Characters: "+text.length());
        }  
      
        public static void main(String[] args) 
        {  
            new Three();  
        }  
      
}
