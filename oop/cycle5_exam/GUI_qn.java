import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.event.*;

public class GUI_qn implements ActionListener {

    private JTextField velKmphField;
    private JTextField velMsField;

    GUI_qn() {
        
        JFrame frame =  new JFrame();
        
        JPanel panel = new JPanel();

        panel.setLayout(null);

        frame.setSize(450,300);
        frame.setDefaultCloseOperation(3);
        frame.setTitle("TVE19CS007 AJAY KRISHNA KV");

        JLabel velKmph = new JLabel("Velocity - KMPH");
        velKmph.setBounds(10, 20, 150, 25);

        velKmphField = new JTextField(20);
        velKmphField.setBounds(170, 20, 165, 25);

        JLabel velMs = new JLabel("Velocity - M/S");
        velMs.setBounds(10, 85, 150, 25);

        velMsField = new JTextField(20);
        velMsField.setBounds(170, 85, 165, 25);

        JButton button = new JButton("Convert");
        button.setBounds(100, 50, 120, 25);
        button.addActionListener(this);

        panel.add(velKmph); panel.add(velKmphField); 
        panel.add(button);
        panel.add(velMs); panel.add(velMsField);
        frame.add(panel);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String kmph = velKmphField.getText();
        double ms = Double.parseDouble(kmph) * (5.0/18.0);

        velMsField.setText(Double.toString(ms));
    }

    public static void main(String[] args) {
        new GUI_qn();
    }
}