import java.sql.*; 
import java.util.*;
 
public class JDBC_qn {  
   public static void main(String[] args)throws Exception {
      Class.forName("com.mysql.cj.jdbc.Driver");

      System.out.println("\nProgram by AJAY KRISHNA KV\n");
      
      try (

         Scanner in = new Scanner(System.in);
         Connection conn = DriverManager.getConnection(
               "jdbc:mysql://localhost:3306/dictionary",
               "ajayk", "password123");

         Statement stmt = conn.createStatement();
         
      ) {
         System.out.println("Enter word to find meaning");
         String word = in.nextLine();

         String strSelect = "select meaning from words where word = \""+word+"\"";
 
         ResultSet rset = stmt.executeQuery(strSelect);

         if (rset.next()) {
            System.out.print("The word's meaning is: ");
            String meaning = rset.getString("meaning");
            System.out.println(meaning+"\n");            
         }
         else {
            System.out.println("Word not found\n");
         }

      } catch(SQLException ex) {
         ex.printStackTrace();
      }
   }
}