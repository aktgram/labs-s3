import java.util.*;

class BinarySearch
{
    static int binarysearch(int arr[],int n,int key)
    {
        int pos=-1,low,high,mid;
        low=0;
        high=n-1;

        while(low<=high)
        {
            mid=(low+high)/2;

            if(arr[mid]==key)
            {
                pos=mid+1;
                break;
            }         
            
            else if(arr[mid]<key)
                low=mid+1;

            else
                high=mid-1;
        }
        return pos;
    }

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n,key,pos;

        System.out.print("Enter the number of elements:");
        n=scan.nextInt();

        int arr[] = new int[n];

        System.out.println("Enter the elements:");
        for(int i=0;i<n;++i)
            arr[i]=scan.nextInt();

        Arrays.sort(arr);

        System.out.print("\nEnter the element to search:");
        key=scan.nextInt();

        scan.close();

        pos=binarysearch(arr,n,key);

        System.out.println("Sorted Array:");
        for(int i=0;i<n;++i)
            System.out.print(arr[i]+" ");

        if(pos==-1)
            System.out.println("\n\nElement "+key+" not found");
        else
            System.out.println("\n\nElement "+key+" found in position "+pos);
        
    }
}