import java.util.*;

public class DoublyLinkedList 
{
    class Node
    {
        int data;
        Node next;
        Node prev;

        Node(int data)
        {
            this.data=data;
        }
    }

    Node head=null,tail=null; 

    public void addNode(int data)
    {
        Node tmp = new Node(data);
        tmp.prev=tmp.next=null;

        if(head==null)
            head=tail=tmp;
        
        else
        {
            tail.next=tmp;
            tmp.prev=tail;
            tail=tmp;
        }
    }

    public void deleteNode(int data)
    {
        if(head==null)
        {
            System.out.println("\nList Empty");        
            return;
        }

        int flag=-1;

        if(head.data==data)
        {
            head=head.next;
            head.prev=null;
            flag=1;
        }

        else if(tail.data==data)
        {
            tail=tail.prev;
            tail.next=null;
            flag=1;
        }


        else
        {
            Node tmp;

            tmp=head;
            
            while(tmp!=null)
            {
                if(tmp.data==data)
                {
                    if(tmp.prev!=null)
                        tmp.prev.next=tmp.next;
                    
                    if(tmp.next!=null)
                        tmp.next.prev=tmp.prev;

                    flag=1;
                    break;
                }
                tmp=tmp.next;
            }
        }

        if(flag==-1)
            System.out.println("Element "+data+" not found");

        else
            System.out.println("Element "+data+" deleted");
    }

    public void display()
    {
        Node tmp = head;

        while(tmp!=null)
        {
            System.out.print(tmp.data+" ");
            tmp=tmp.next;
        }
        System.out.print("\n");
    }

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int ch,val;
        DoublyLinkedList Dlist = new DoublyLinkedList();        

        do
        {
            System.out.println("\n1.Insert a node   2.Delete a node   3.Display the list    4.Exit");
            System.out.print("Enter your choice:");
            ch=scan.nextInt();

            switch(ch)
            {
                case 1:System.out.print("Enter node to insert:");
                       val=scan.nextInt();
                       Dlist.addNode(val);
                       break;

                case 2:System.out.print("Enter node to delete:");
                       val=scan.nextInt();
                       Dlist.deleteNode(val);
                       break;
                
                case 3:Dlist.display();
                       break;
            }
        }while(ch!=4);
        scan.close();
    }
}
