import java.util.*;

public class Four {
    void heap_sort(int ar[],int n){
        
        // creating the Heap
        for (int i = 1; i<=n; i++) {
            int parent = (i+1)/2 - 1;
            int item = ar[i];
            int j = i;
            while (parent >= 0 && item > ar[parent]) {
                ar[j] = ar[parent];
                j = parent;
                parent = (j+1)/2 - 1;
            }
            ar[j] = item;
        }
        
        //sorting using delete heap
        int root = ar[0];

        while (n>0) {

            int t = ar[0];
            ar[0] = ar[n];
            ar[n] = t;


            n--;
            int item = ar[0];        
            int i = 0, left = 1, right = 2;
            while(right <= n) {
                
                if (item > ar[left] && item > ar[right]) {
                    ar[i] = item;
                    break;
                }
                
                if (ar[left] >= ar[right]) {
                    ar[i] = ar[left];
                    i = left;
                }
                else {
                    ar[i] = ar[right];
                    i = right;
                }
                
                left = (i+1)*2 - 1;
                right = (i+1)*2;
                
            }
            
            if (left == n && ar[left] > item ) {
                ar[i] = ar[left];
                i = left;
            }
            ar[i] = item;
        } 
    }
        
    public static void main(String[] args) {
        Four hp = new Four();
        int n;
        Scanner in =new Scanner(System.in);

        System.out.print("Enter the number of elements: ");
        n=in.nextInt();
        
        int[] ar=new int[n];

        System.out.println("Enter the  elements: ");
        for(int i=0;i<n;i++){
            ar[i]=in.nextInt();
        }

        hp.heap_sort(ar, n-1);
        
        System.out.print("The sorted array: ");
        for(int i=0;i<n;i++){
            System.out.print( ar[i]+" ");
        }
        System.out.println();
    }
}
