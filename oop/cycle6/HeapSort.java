import java.util.*;

class HeapSort 
{
    static void heapify(int arr[],int n,int i) 
    {
        int big=i;
        int left=2*i+1;
        int right=2*i+2;
        int tmp;
  
        if (left<n && arr[left]>arr[big])
            big=left;

        if (right<n && arr[right]>arr[big])
            big=right;
        
        if(big!=i) 
        {
            tmp=arr[i];
            arr[i]=arr[big];
            arr[big]=tmp;
            
            heapify(arr,n,big);
        }
    }

    static void heapsort(int arr[],int n)
    {
        int i,tmp;
        for(i=(n+1)/2-1;i>=0;i--)
          heapify(arr,n+1,i);
      
        for(i=n;i>=0;i--) 
        {
            tmp=arr[0];
            arr[0]=arr[i];
            arr[i]=tmp;
            heapify(arr,i,0);
        }
    }
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n;

        System.out.print("Enter the number of elements:");
        n=scan.nextInt();

        int arr[] = new int[n];

        System.out.println("Enter the elements:");
        for(int i=0;i<n;++i)
            arr[i]=scan.nextInt();  
            
        scan.close();
            
        heapsort(arr,n-1);

        System.out.println("Sorted Array:");
        for(int i=0;i<n;++i)
            System.out.print(arr[i]+" ");

        System.out.print("\n");
    }
}
