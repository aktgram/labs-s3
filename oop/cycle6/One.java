import java.util.*;

class One {
    static class Node {
        int data;
        Node next, prev;

        Node(int data) {
            this.data =  data;
            next = prev = null;           
        }
    }

    static Node insertNode(Node header, int value) {
        Node insert = new Node(value);
        Node ptr = header;
        while(ptr.next != null) {
            ptr = ptr.next;
        }
        ptr.next = insert;
        insert.prev = ptr;
        return insert;
    }

    static void deleteNode(Node header, int value) {
        if(header.next == null) {
            System.out.println("List is empty");
        }
        else{
            Node ptr = header;
            while (ptr.next != null) {
                Node ptr1 = ptr;
                ptr = ptr.next;
                if (ptr.data == value) {
                    try {
                        ptr.next.prev = ptr1;
                        ptr1.next = ptr.next;    
                    } catch (Exception e) {
                        ptr1.next = null;
                    }
                    return;
                }
            }
            System.out.println("Node not found");
        }
    }

    static void display(Node header) {
        if (header.next == null) {
            System.out.println("List empty");
        }
        else {
            Node ptr = header;
            while (ptr.next != null) {
                ptr = ptr.next;
                System.out.print(ptr.data+" ");
            }
            System.out.println();
        }
    }

    static void revdisplay(Node header, Node tail) {
        if (header.next == null) {
            System.out.println("List empty");
        }
        else {
            Node ptr = tail;
            while (ptr != header) {
                System.out.print(ptr.data+" ");
                ptr = ptr.prev;
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        Node header = new Node(0);
        Node tail = null;
        Scanner in = new Scanner(System.in);
        System.out.println("Choose an option:\n1. Insert an element");
        System.out.println("2. Delete an element\n3. Display in order");
        System.out.println("4. Display in reverse order\n5. Exit");
        int c = in.nextInt();
        while (c != 5) {
            switch (c) {
                case 1:
                    System.out.print("Enter the element: ");
                    tail = insertNode(header, in.nextInt());
                    break;
                case 2:
                    System.out.print("Enter the element: ");
                    deleteNode(header, in.nextInt());
                    break;
                case 3:
                    display(header);
                    break;
                case 4:
                    revdisplay(header, tail);
                    break;
                default: 
                    System.out.println("Wrong choice!!");
            }
            System.out.print("Choose an option: ");
            c = in.nextInt();
        }

        in.close();
    }
}