import java.util.*;

class QuickSort 
{
    public static int partition(String arr[],int low,int high)
    {
        String pivot=arr[high];
        int index=low;
        String tmp;
        
        for(int i=low;i<high;++i)
            if(arr[i].compareTo(pivot)<0)
            {
                tmp=arr[i];
                arr[i]=arr[index];
                arr[index]=tmp;
                index++;
            }
            
        tmp=arr[index];
        arr[index]=arr[high];
        arr[high]=tmp;
            
        return index;   
    }

    public static void quicksort(String arr[],int low,int high)
    {
        int pivot;
    
        if(high>low)
        {
            pivot=partition(arr,low,high);
            
            quicksort(arr,0,pivot-1);
            quicksort(arr,pivot+1,high);
        }
    }
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n;

        System.out.print("Enter the number of names:");
        n=scan.nextInt();
        scan.nextLine();

        String[] arr = new String[n];

        System.out.println("Enter the names:");
        for(int i=0;i<n;++i)
            arr[i]=scan.nextLine();

        scan.close();

        quicksort(arr,0,n-1);

        System.out.println("\nSorted list of names:");
        for(int i=0;i<n;++i)
            System.out.print(arr[i]+"\n");
    }
    
}
