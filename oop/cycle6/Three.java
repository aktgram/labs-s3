import java.util.*;

public class Three {

    static int partition(String ar[], int pivot, int h) {
    
        int i = h+1;
        int j = h;
        while (j != pivot) {
            if(ar[j].compareTo(ar[pivot]) > 0) {

                i--;

                String t = ar[j];
                ar[j] = ar[i];
                ar[i] = t;
            }    
            j--;
        }
        i--;

        String t = ar[j];
        ar[j] = ar[i];
        ar[i] = t;

        return i;
    }
    
    
    static void quick_sort(String ar[],int l, int h){
        
        if (l<h) {
                
            int pivot = partition(ar, l, h);
            
            quick_sort(ar, l, pivot-1);
            quick_sort(ar, pivot+1, h);
            
        }
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("How many names: ");
        int n = in.nextInt();
        
        System.out.println("Enter the names: ");
        String ar[]=new String[n];
        in.nextLine();

        for (int i = 0; i < n; i++) {
            ar[i] = in.nextLine();
        }

        quick_sort(ar, 0, n - 1);

        System.out.println("\nSorted list is: ");
        for (int i = 0; i <n; i++) {
            System.out.println(ar[i]+" ");
        }
    }
}
