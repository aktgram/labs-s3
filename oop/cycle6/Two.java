import java.util.*;

public class Two {
    static int bin_search(int arr[],int n,int search)
    {
        int pos=-1,low,high,mid;
        low=0;
        high=n-1;

        while(low<=high)
        {
            mid=(low+high)/2;

            if(arr[mid]==search)
            {
                pos=mid+1;
                break;
            }         
            
            else if(arr[mid]<search)
                low=mid+1;

            else
                high=mid-1;
        }
        return pos;
    }

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n,search,pos;

        System.out.print("How many numbers?: ");
        n=in.nextInt();

        int arr[] = new int[n];

        System.out.println("Enter the numbers in sorted form:");
        for(int i = 0; i < n; i++)
            arr[i]=in.nextInt();

        System.out.print("\nEnter number to search: ");
        search=in.nextInt();

        in.close();

        pos = bin_search(arr,n,search);

        if(pos==-1)
            System.out.println("\nThe Number "+search+" not found");
        else
            System.out.println("\nThe Number "+search+" found in position "+pos);
        
    }
}
