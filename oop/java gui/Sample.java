import java.awt.*;

class Sample extends Frame {
   
    Sample() {
        Button b = new Button("Click this");
        b.setBounds(200, 100, 80, 40);
        add(b);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        Sample ob = new Sample();
    }
}