import java.awt.*;
import java.awt.event.*;

public class Second extends Frame implements ActionListener {
    TextField tf;
    
    Second() {

        //creating components
        tf = new TextField();
        tf.setBounds(60, 50, 170, 20);
        Button b = new Button("Click me");
        b.setBounds(100, 120, 80, 30);

        Button bt = new Button("Click me2");
        bt.setBounds(0, 40, 60, 40);

        //register listener
        b.addActionListener(this); //passing current instance
        bt.addActionListener(this);

        //add components and set size, layout and visibilty
        add(b); add(bt);
        add(tf);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        tf.setText("Hello world");
    }

    public static void main(String[] args) {
        new Second();
    }
}
