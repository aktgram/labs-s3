import java.awt.*;
import java.awt.event.*;

public class awtlogin implements ActionListener, TextListener {

    private TextField userField;
    private TextField pwdField;
    private Label loggedin;
    private String password = "";

    awtlogin() {
        
        Frame frame =  new Frame();
        Panel panel = new Panel();

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);            
            }
        });
        panel.setLayout(null);

        frame.setSize(450,300);
        
        frame.setTitle("Login bitch");

        Label user = new Label("User");
        user.setBounds(10, 20, 80, 25);

        userField = new TextField(20);
        userField.setBounds(100, 20, 165, 25);

        Label pwd = new Label("Password");
        pwd.setBounds(10, 50, 80, 25);

        pwdField = new TextField(20);
        pwdField.addTextListener(this);
        pwdField.setBounds(100, 50, 165, 25);

        Button button = new Button("Login");
        button.setBounds(100, 85, 80, 25);
        button.addActionListener(this);

        loggedin = new Label("");
        loggedin.setBounds(20, 120, 300, 25);

        panel.add(user); panel.add(userField); panel.add(pwd); panel.add(pwdField);
        panel.add(button); panel.add(loggedin);
        frame.add(panel);
        frame.setVisible(true);

    }

    @Override
    public void textValueChanged(TextEvent e) {
        String text = pwdField.getText();
        int length = text.length();

        // handling backspaces and retype of password
        if(this.password.length() > length) {
            if(length > 0)
                this.password = this.password.substring(0, length);
            else
                this.password = "";
        }

        //adding characters typed into password field into password variable
        //& making value inside actual text field (*)'s like a pwd
        else {
            this.password += text.charAt(length - 1);
            this.password = this.password.replace("*", "");
            
            if(length == 1) {
                text = "*";
            }
            else {
                text = text.substring(0,length - 1);
                text = text.concat("*");
            }
        }
        
        pwdField.setText(text);
        pwdField.setCaretPosition(length);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String username = userField.getText();
        String password = this.password;
        String display = "Username: "+username+"   \nPassword: "+password;

        System.out.println(display);
        loggedin.setText(display);
        
    }

    public static void main(String[] args) {
        new awtlogin();
    }
}

