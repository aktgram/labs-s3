import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class swing1 implements ActionListener {

    int count = 0;
    JLabel label;

    swing1() {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        frame.setSize(500, 300);

        JButton button = new JButton("click me");
        label = new JLabel("Number of clicks: 0");
        button.addActionListener(this);

        panel.add(button);
        panel.add(label);

        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("A fucking java ui");
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        count++;
        label.setText("Number of clicks: "+count);
    }

    public static void main(String[] args) {
        new swing1();
    }
}