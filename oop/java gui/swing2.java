import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.event.*;

public class swing2 implements ActionListener {

    private JTextField userField;
    private JPasswordField pwdField;
    private JLabel loggedin;

    swing2() {
        
        JFrame frame =  new JFrame();
        JPanel panel = new JPanel();

        panel.setLayout(null);

        frame.setSize(450,300);
        frame.setDefaultCloseOperation(3);
        frame.setTitle("Login bitch");

        JLabel user = new JLabel("User");
        user.setBounds(10, 20, 80, 25);

        userField = new JTextField(20);
        userField.setBounds(100, 20, 165, 25);

        JLabel pwd = new JLabel("Password");
        pwd.setBounds(10, 50, 80, 25);

        pwdField = new JPasswordField(20);
        pwdField.setBounds(100, 50, 165, 25);

        JButton button = new JButton("Login");
        button.setBounds(100, 85, 80, 25);
        button.addActionListener(this);

        loggedin = new JLabel("");
        loggedin.setBounds(20, 120, 300, 25);

        panel.add(user); panel.add(userField); panel.add(pwd); panel.add(pwdField);
        panel.add(button); panel.add(loggedin);
        frame.add(panel);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String username = userField.getText();
        String password = new String(pwdField.getPassword());
        String display = "Username: "+username+"   \nPassword: "+password;

        System.out.println(display);
        loggedin.setText(display);
        
    }

    public static void main(String[] args) {
        new swing2();
    }
}
