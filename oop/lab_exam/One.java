import java.io.*;
import java.util.*;

/* 
    Name: Ajay Krishna K V
    KTU Id: TVE19CS007
    Roll no: 7
*/

class Sorting {

    static void swap(int left, int right, int[] s) {
        int temp;
        temp = s[left];
        s[left] = s[right];
        s[right] = temp;
    }

    static int partition(int left, int right, int[] quick) {
        int loc = left;
        while (left < right) {
            while ((quick[loc] <= quick[right]) && (loc < right)) {
                right--;
            }
            if (quick[loc] > quick[right]) {
                swap(loc, right, quick);
                loc = right;
                left++;
            }
            while ((quick[loc] >= quick[left]) && (loc > left)) {
                left++;
            }
            if (quick[loc] < quick[left]) {
                swap(loc, left, quick);
                loc = left;
                right--;
            }
        }
        return loc;
    }

    static void quicksorting(int low, int high, int[] quick) {
        int loc, left = low, right = high;
        if (left < right) {
            loc = partition(left, right, quick);
            quicksorting(left, loc - 1, quick);
            quicksorting(loc + 1, right, quick);
        }

    }

    synchronized void bubblesort(int[] arr, int n) {
        try {
            FileWriter fw = null;

            try {
                fw = new FileWriter("sorted_output.txt", true);
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }

            BufferedWriter bw = new BufferedWriter(fw);

            int bsort[] = arr;
            for (int round = 1; round < n; round++) {
                for (int i = 0; i < n - round; i++) {
                    if (bsort[i] < bsort[i + 1]) {
                        swap(i, i + 1, bsort);
                    }
                }
            }
            System.out.print("Bubble sorted: ");

            try {
                bw.write("Bubble Sorted: ");
                for (int i : bsort) {
                    bw.write(Integer.toString(i) + " ");
                    System.out.print(i + " ");
                }
                bw.newLine();
            } catch (IOException e) {
                System.out.println(e);
            }

            System.out.println();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e);
            }

            bw.flush();
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    synchronized void quicksort(int[] arr, int n) {
        try {
            FileWriter fw = null;

            try {
                fw = new FileWriter("sorted_output.txt", true);
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }

            BufferedWriter bw = new BufferedWriter(fw);

            int qsort[] = arr;
            quicksorting(0, n - 1, qsort);
            System.out.print("Quick sorted: ");

            try {
                bw.write("Quick Sorted: ");
                for (int i : qsort) {
                    bw.write(Integer.toString(i) + " ");
                    System.out.print(i + " ");
                }
                bw.newLine();
            } catch (IOException e) {
                System.out.println(e);
            }

            System.out.println();

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e);
            }

            bw.flush();
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

class BubbleSortThread extends Thread {
    int n;
    int[] arr;
    Sorting sort;

    BubbleSortThread(Sorting sort, int n, int[] arr) {
        this.sort = sort;
        this.n = n;
        this.arr = arr;
    }

    public void run() {
        sort.bubblesort(arr, n);
    }
}

class QuickSortThread extends Thread {
    int n;
    int[] arr;
    Sorting sort;

    QuickSortThread(Sorting sort, int n, int[] arr) {
        this.sort = sort;
        this.n = n;
        this.arr = arr;
    }

    public void run() {
        sort.quicksort(arr, n);
    }
}

class One {
    public static void main(String[] args) throws Exception {
        try {
            Scanner in = new Scanner(System.in);
            FileWriter fw = null;
            FileReader fr = null;

            try {
                try {
                    fw = new FileWriter("sorted_output.txt", true);
                } catch (FileNotFoundException e) {
                    System.out.println(e);
                }

                fw.write("\n");
                fw.close();
            } catch (IOException e) {
                System.out.println(e);
            }

            System.out.print("How many numbers? ");
            int n = in.nextInt();
            int arr[] = new int[n];
            System.out.print("Input numbers: ");
            for (int i = 0; i < n; i++) {
                arr[i] = in.nextInt();
                if (arr[i] < 0) {
                    throw new ArithmeticException("Positive Integers only!");
                }
            }

            Sorting sort = new Sorting();
            QuickSortThread qt = new QuickSortThread(sort, n, arr);
            BubbleSortThread bt = new BubbleSortThread(sort, n, arr);

            qt.start();
            bt.start();

            qt.join();
            bt.join();

            try {
                try {
                    fr = new FileReader("sorted_output.txt");
                } catch (FileNotFoundException e) {
                    System.out.println(e);
                }

                int k = fr.read();

                System.out.println("\nContents of the file: ");
                while (k != -1) {
                    System.out.print((char) k);
                    k = fr.read();
                }
                fr.close();
            } catch (IOException e) {
                System.out.println(e);
            }

            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}