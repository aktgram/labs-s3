import java.sql.*; 
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/* 
    Name: Ajay Krishna K V
    KTU Id: TVE19CS007
    Roll no: 7
*/

class jdbc {  

    public static void signup(String username, String password)throws Exception {
       Class.forName("com.mysql.cj.jdbc.Driver");
       try (
          
          Scanner input = new Scanner(System.in);
          Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/java_cycle5",
                "ajayk", "password123");
 
          Statement stmt = conn.createStatement();
          ) {
             
            String table_name = "SignUp";
             try {
                String create = "CREATE TABLE "+table_name
                              +"(username VARCHAR(20), password VARCHAR(20));";
                stmt.executeUpdate(create);
             }
             catch (Exception e) {
                // System.out.println("Table exists, now enter data");
                // table exists
             }
 
            String insert = "INSERT INTO "+table_name+
                            " (username, password) VALUES ('"
                                +username+"','"+password+"');";
            stmt.executeUpdate(insert);
            
       } catch(SQLException ex) {
          ex.printStackTrace();
       }
    }

    public static int login(String username, String password) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
       try (
          
          Scanner input = new Scanner(System.in);
          Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/java_cycle5",
                "ajayk", "password123");
 
          Statement stmt = conn.createStatement();
          ) {

            String table_name = "SignUp";
             try {
                String create = "CREATE TABLE "+table_name
                              +"(username VARCHAR(20) NOT NULL PRIMARY KEY, password VARCHAR(20));";
                stmt.executeUpdate(create);
             }
             catch (Exception e) {
                // System.out.println("Table exists, now enter data");
                // table exists
             }

             String strSelect = "select * from "+table_name+" where username = '"+
             username+"' AND password = '"+password+"'";
             System.out.println("The SQL statement is: " + strSelect + "\n");
     
             ResultSet rset = stmt.executeQuery(strSelect);
             int rowCount = 0;
             while(rset.next()) { 
                 String name = rset.getString("username");
                 String pwd = rset.getString("password");
                 System.out.println( username + ", " + password);
                 ++rowCount;
             }
             System.out.println("Total number of records = " + rowCount);

             if(rowCount == 0) {
                 return 0;
             }
             else {
                 return 1;
             }

       } catch(SQLException ex) {
          ex.printStackTrace();
          return -1;
       }
    }
 }

class awtlogin implements ActionListener {

    private JTextField userField;
    private JPasswordField pwdField;
    private JLabel loggedin;

    awtlogin() {
        
        JFrame frame =  new JFrame();
        JPanel panel = new JPanel();

        panel.setLayout(null);

        frame.setSize(450,300);
        frame.setDefaultCloseOperation(3);
        frame.setTitle("Login by Ajay Krishna K V");

        JLabel user = new JLabel("User");
        user.setBounds(10, 20, 80, 25);

        userField = new JTextField(20);
        userField.setBounds(100, 20, 165, 25);

        JLabel pwd = new JLabel("Password");
        pwd.setBounds(10, 50, 80, 25);

        pwdField = new JPasswordField(20);
        pwdField.setBounds(100, 50, 165, 25);

        JButton button = new JButton("Login");
        button.setBounds(100, 85, 80, 25);
        button.addActionListener(this);

        loggedin = new JLabel("");
        loggedin.setBounds(20, 130, 300, 25);

        panel.add(user); panel.add(userField); panel.add(pwd); panel.add(pwdField);
        panel.add(button); panel.add(loggedin);
        frame.add(panel);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String username = userField.getText();
        String password = new String(pwdField.getPassword());

        if(username.equals("") || password.equals("")) {
            loggedin.setText("A field blank");
            return;
        }

        try {
            jdbc db = new jdbc();
            int success = jdbc.login(username, password);
            if(success == 1) {
                loggedin.setText("Log in success");
            }
            else if(success == 0) {
                loggedin.setText("Log in failed");
            }
            else {
                loggedin.setText("database error");
            } 
        } catch (Exception ex) {
            ex.printStackTrace();
            loggedin.setText("database error");
        }
        
    }
}

class awtsignup implements ActionListener {

    private JTextField userField;
    private JPasswordField pwdField;
    private JPasswordField cpwdField;
    private JLabel success;
    JFrame frame;

    awtsignup() {
        
        frame =  new JFrame();
        JPanel panel = new JPanel();

        panel.setLayout(null);

        frame.setSize(450,300);
        frame.setDefaultCloseOperation(3);
        frame.setTitle("SignUp by Ajay Krishna K V");

        JLabel signup = new JLabel("Sign Up");
        signup.setBounds(10, 20, 80, 25);

        JLabel user = new JLabel("Username");
        user.setBounds(10, 60, 150, 25);

        userField = new JTextField(20);
        userField.setBounds(160, 60, 165, 25);

        JLabel pwd = new JLabel("Password");
        pwd.setBounds(10, 100, 150, 25);

        pwdField = new JPasswordField(20);
        pwdField.setBounds(160, 100, 165, 25);

        JLabel cpwd = new JLabel("Confirm Password");
        cpwd.setBounds(10, 130, 150, 25);

        cpwdField = new JPasswordField(20);
        cpwdField.setBounds(160, 130, 165, 25);

        JButton button = new JButton("Sign Up");
        button.setBounds(100, 165, 100, 25);
        button.addActionListener(this);

        JButton lbutton = new JButton("Go to Login");
        lbutton.setBounds(220, 165, 140, 25);
        lbutton.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println("Direct login");
                    new awtlogin();  
                    frame.setVisible(false);
                }
            }
        );

        success = new JLabel("");
        success.setBounds(20, 200, 300, 25);

        panel.add(signup); panel.add(user); panel.add(userField); panel.add(pwd);
        panel.add(pwdField); panel.add(cpwd); panel.add(cpwdField);
        panel.add(button); panel.add(lbutton); panel.add(success);
        frame.add(panel);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String username = userField.getText();
        String password = new String(pwdField.getPassword());
        String cpassword = new String(cpwdField.getPassword());

        if(username.equals("") || password.equals("") || cpassword.equals("")) {
            success.setText("A field blank");
        }

        else if(password.equals(cpassword)) {
            try {
                success.setText("");
                jdbc db = new jdbc();
                db.signup(username, password);
                new awtlogin();  
                frame.setVisible(false);  
            } catch (Exception ex) {
                ex.printStackTrace();
                success.setText("database error!");
            }
        }
        else {
            success.setText("Passwords dont match!!");
        }
        
    }

}


class Three {
    Three () {
        new awtsignup();
    }
    public static void main(String args[]) {
        new Three();
    }
}