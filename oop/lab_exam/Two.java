import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;

public class Two implements ActionListener {

    JFrame frame;
    JPanel panel, inputselect, outputselect;
    JLabel name, imode, omode;
    JRadioButton i1, i2, i3, o1, o2, o3;
    JButton convert;
    JTextField input, output;
    ButtonGroup inputradio, outputradio;

    Two() {
        // Declaring Frame size
        frame = new JFrame("Temperature converter");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);

        // Name Label
        name = new JLabel("by Sudev Suresh Sreedevi", SwingConstants.CENTER);
        frame.add(name, BorderLayout.SOUTH);

        // Complete Window Panel
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        frame.add(panel);

        // Input Text Field
        input = new JTextField("0");
        input.setColumns(11);
        panel.add(input);

        // radio buttons
        i1 = new JRadioButton("Celsius");
        i2 = new JRadioButton("Fahrenheit");
        i3 = new JRadioButton("Kelvin");

        o1 = new JRadioButton("Celsius");
        o2 = new JRadioButton("Fahrenheit");
        o3 = new JRadioButton("Kelvin");

        // ActionCommand to differentiate between input radio and output radio
        i1.setActionCommand("iC");
        i2.setActionCommand("iF");
        i2.setActionCommand("iK");
        o1.setActionCommand("oC");
        o2.setActionCommand("oF");
        o3.setActionCommand("oK");

        // eventlisteners for radio buttons
        i1.addActionListener(this);
        i2.addActionListener(this);
        i3.addActionListener(this);
        o1.addActionListener(this);
        o2.addActionListener(this);
        o3.addActionListener(this);

        // radio button groups
        inputradio = new ButtonGroup();
        outputradio = new ButtonGroup();

        // adding to button groups
        inputradio.add(i1);
        inputradio.add(i2);
        inputradio.add(i3);
        outputradio.add(o1);
        outputradio.add(o2);
        outputradio.add(o3);

        // independent panels for arranging radio buttons in separate rows
        inputselect = new JPanel();
        outputselect = new JPanel();

        // labels for button groups
        imode = new JLabel("Input Unit: ", SwingConstants.CENTER);
        omode = new JLabel("Output Unit: ", SwingConstants.CENTER);

        // adding radio buttons and labels into their respective panels
        inputselect.add(imode);
        inputselect.add(i1);
        inputselect.add(i2);
        inputselect.add(i3);
        outputselect.add(omode);
        outputselect.add(o1);
        outputselect.add(o2);
        outputselect.add(o3);

        // adding the panels to the main panel
        panel.add(inputselect);
        panel.add(outputselect);

        // output field
        output = new JTextField("");
        output.setColumns(11);
        output.setEditable(false);
        panel.add(output);

        convert = new JButton("Convert");

        convert.addActionListener(this);

        panel.add(convert);

        frame.setVisible(true);
    }

    // Conversion functions
    public float ctof(float c) {
        float f = ((9 / 5) * c) + 32;
        return f;
    }

    public float ftoc(float f) {
        float c = (5 / 9) * (f - 32);
        return c;
    }

    public float ktoc(float k) {
        float c = k - 273;
        return c;
    }

    public float ctok(float c) {
        float k = c + 273;
        return k;
    }

    public float ftok(float f) {
        float k = ((f - 32) * (5 / 9)) + 273;
        return k;
    }

    public float ktof(float k) {
        float f = ((k - 273) * (9 / 5)) + 32;
        return f;
    }

    public void actionPerformed(ActionEvent e) {
        String msg = "";
        msg = e.getActionCommand();
        DecimalFormat df = new DecimalFormat(" 0.000 ");
        float in = Float.parseFloat(input.getText());
        if (msg == "Convert") {
            String inpmode = inputradio.getSelection().getActionCommand();
            String outmode = outputradio.getSelection().getActionCommand();
            if (inpmode == "iC" && outmode == "oF") {
                output.setText("" + ctof(in));
            } else if (inpmode == "iF" && outmode == "oC") {
                output.setText("" + ftoc(in));
            } else if (inpmode == "iC" && outmode == "oK") {
                output.setText("" + ctok(in));
            } else if (inpmode == "iK" && outmode == "oC") {
                output.setText("" + ktoc(in));
            } else if (inpmode == "iF" && outmode == "oK") {
                output.setText("" + ftok(in));
            } else if (inpmode == "iK" && outmode == "oF") {
                output.setText("" + ktof(in));
            }
        }
    }

    public static void main(String args[]) {
        new Two();
    }
}