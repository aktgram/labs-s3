import java.io.FileReader;
import java.util.Scanner;

class First {
    public static void main(String[] args) throws Exception {
        try {
            Scanner input =  new Scanner(System.in);
            FileReader fr = new FileReader("out.txt");
            FileWriter fw = new FileWriter("out.txt");

            System.out.println("How many nos? ");
            int n = input.nextInt();

            System.out.println("Enter nos ");
            int arr[] = new int[n];
            for(int i =0; i<n; i++) {
                arr[i] = input.nextInt();
                if(arr[i] < 0) {
                    throw new ArithmeticException("Numbers should be positive!");
                }
            }

            


            fr.close();
            fw.close();
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}